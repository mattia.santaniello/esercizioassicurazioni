import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.Comparator;


public class GestioneAssicurazioni{
    private ArrayList<Assicurazione> listaAssicurazioni;

    public GestioneAssicurazioni(){
        this.listaAssicurazioni = new ArrayList<Assicurazione>();
    }

    public GestioneAssicurazioni(GestioneAssicurazioni g){
        listaAssicurazioni=new ArrayList<Assicurazione>();
        for(int i=0; i<g.getSize(); i++){
            listaAssicurazioni.add(g.get(i).clone());
        }
    }

    public boolean add(Assicurazione a){
        boolean risultato=true;
        int i=0;

        while(i<listaAssicurazioni.size()){
            if(listaAssicurazioni.get(i).equals(a)){
                risultato = false;
                break;
            }
            i++;
        }

        if(risultato)
            listaAssicurazioni.add(a.clone());

        return risultato;
    }

    public int getSize(){return listaAssicurazioni.size();}
    public Assicurazione get(int index){return listaAssicurazioni.get(index);}

    public boolean edit(Assicurazione a){
        boolean risultato=false;
        int i=0;

        while(i<listaAssicurazioni.size()){
            if(listaAssicurazioni.get(i).equals(a)){
                risultato=true;
                listaAssicurazioni.set(i,a);
                break;
            }
            i++;
        }

        return risultato;
    }

    public GestioneAssicurazioni clone(){return new GestioneAssicurazioni(this);}

    public boolean remove(Assicurazione a){return listaAssicurazioni.remove(a);}


    public boolean loadCSV(String filename)throws IOException{
        boolean risultato=false;
        Scanner in = new Scanner(new File(filename)).useDelimiter(System.lineSeparator());
        try {
            in.nextLine();
            while (in.hasNext()) {
                final String[] valori = in.nextLine().split(",");
               
                listaAssicurazioni.add(new Assicurazione(Integer.valueOf(valori[0]),valori[1],valori[2],Double.valueOf(valori[3]),Double.valueOf(valori[4]),Double.valueOf(valori[5]),Double.valueOf(valori[6]),
                                        Double.valueOf(valori[7]),Double.valueOf(valori[8]),Double.valueOf(valori[9]),Double.valueOf(valori[10]),Double.valueOf(valori[11]),Double.valueOf(valori[12]),Double.valueOf(valori[13]),
                                        Double.valueOf(valori[14]),valori[15],valori[16],Integer.valueOf(valori[17])));
            }
            risultato=true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            in.close();
        }

        return risultato; 

    }

    public boolean save(String filename){
        boolean saved=false;
        try{
            FileOutputStream fOut = new FileOutputStream(new File(filename));
            ObjectOutputStream scrivi = new ObjectOutputStream(fOut);
            scrivi.writeObject(listaAssicurazioni);
            saved=true;
        }catch(Exception e){
            e.printStackTrace();
        }

        return saved;
    }

    @SuppressWarnings("unchecked")
    public boolean load(String filename){
        File f = new File(filename);
        boolean loaded = false;

        if(f.exists()){
            try{
                FileInputStream fIn = new FileInputStream(f);
                ObjectInputStream leggi = new ObjectInputStream(fIn);
                listaAssicurazioni=(ArrayList<Assicurazione>)leggi.readObject(); 
                leggi.close();
                fIn.close();
                loaded=true;
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        return loaded;
    }

    @Override
    public String toString(){
        String s="";

        for(Assicurazione i: listaAssicurazioni){
            s+=i.toString()+"\n";
        }

        return s;
    }

    public void sort(){
        Collections.sort(listaAssicurazioni,new Comparator<Assicurazione>(){
            @Override
            public int compare(Assicurazione a,Assicurazione b){
                return a.compareTo(b);
            }
        });
    }
}