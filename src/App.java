public class App {
    public static void main(String[] args) throws Exception {
        GestioneAssicurazioni g = new GestioneAssicurazioni();

        g.loadCSV("FL_insurance_sample.csv");
        g.sort();
        System.out.println(g.get(0).toString());

        TabellaPivot t = new TabellaPivot(g);
        t.conta();
        System.out.println(t.toString());
    }
}
