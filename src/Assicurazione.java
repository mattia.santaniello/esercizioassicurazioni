import java.util.Comparator;

public class Assicurazione{
    private final int policyID;
    private String statecode;
    private String county;
    private double eq_site_limit;
    private double hu_site_limit;
    private double fr_site_limit;
    private double fl_site_limit;
    private double tiv_2011;
    private double tiv_2012;
    private double eq_site_deductible;
    private double hu_site_deductible;
    private double fr_site_deductible;
    private double fl_site_deductible;
    private double point_latitude;
    private double point_longitude;
    private String line;
    private String construction;
    private int point_granularity;

    public Assicurazione(){policyID=0;}

    public Assicurazione(int policyID, String statecode,String county,double eq_site_limit,double hu_site_limit,double fl_site_limit,
                        double fr_site_limit,double tiv_2011,double tiv_2012,double eq_site_deductible, double hu_site_deductible,
                        double fl_site_deductible,double fr_site_deductible,double point_latitude,double point_longitude,String line,String construction,int point_granularity){

        this.policyID=policyID;
        this.statecode=statecode;
        this.county=county;
        this.eq_site_limit=eq_site_limit;
        this.hu_site_limit=hu_site_limit;
        this.fl_site_limit=fl_site_limit;
        this.fr_site_limit=fr_site_limit;
        this.tiv_2011=tiv_2011;
        this.tiv_2012=tiv_2012;
        this.eq_site_deductible=eq_site_deductible;
        this.hu_site_deductible=hu_site_deductible;
        this.fl_site_deductible=fl_site_deductible;
        this.fr_site_deductible=fr_site_deductible;
        this.point_latitude=point_latitude;
        this.point_longitude=point_longitude;
        this.line=line;
        this.construction=construction;
        this.point_granularity=point_granularity;
    }

    public Assicurazione(Assicurazione a){
        this.policyID=a.getPolicyID();
        this.statecode=a.getStateCode();
        this.county=a.getCounty();
        this.eq_site_limit=a.getEqSiteLimit();
        this.hu_site_limit=a.getHuSiteLimit();
        this.fl_site_limit=a.getFlSiteLimit();
        this.fr_site_limit=a.getFrSiteLimit();
        this.tiv_2011=a.getTiv2011();
        this.tiv_2012=a.getTiv2012();
        this.eq_site_deductible=a.getEqSiteDeductible();
        this.hu_site_deductible=a.getHuSiteDeductible();
        this.fl_site_deductible=a.getFlSiteDeductible();
        this.fr_site_deductible=a.getFrSiteDeductible();
        this.point_latitude=a.getPointLatitude();
        this.point_longitude=a.getPointLongitude();
        this.line=a.getLine();
        this.construction=a.getConstruction();
        this.point_granularity=a.getPointGranularity();
    }

    public Assicurazione clone(){return new Assicurazione(this);}

    public void setStateCode(String statecode){this.statecode=statecode;}
    public void setCounty(String county){this.county=county;}
    public void setEqSiteLimit(double eq_site_limit){this.eq_site_limit=eq_site_limit;}
    public void setHuSiteLimit(double hu_site_limit){this.hu_site_limit=hu_site_limit;}
    public void setFlSiteLimit(double fl_site_limit){this.fl_site_limit=fl_site_limit;}
    public void setTiv2011(double tiv_2011){this.tiv_2011=tiv_2011;}
    public void setTiv2012(double tiv_2012){this.tiv_2012=tiv_2012;}
    public void setEqSiteDeductible(double eq_site_deductible){this.eq_site_deductible=eq_site_deductible;}
    public void setHuSiteDeductible(double hu_site_deductible){this.hu_site_deductible=hu_site_deductible;}
    public void setFlSiteDeductible(double fl_site_deductible){this.fl_site_deductible=fl_site_deductible;}
    public void setFrSiteDeductible(double fr_site_deductible){this.fr_site_deductible=fr_site_deductible;}
    public void setPointLatitude(double point_latitude){this.point_latitude=point_latitude;}
    public void setPointLongitude(double point_longitude){this.point_longitude=point_longitude;}
    public void setLine(String line){this.line=line;}
    public void setConstruction(String construction){this.construction=construction;}
    public void setGranularity(int point_granularity){this.point_granularity=point_granularity;}

    public int getPolicyID(){return policyID;}
    public String getStateCode(){return statecode;}
    public String getCounty(){return county;}
    public double getEqSiteLimit(){return eq_site_limit;}
    public double getHuSiteLimit(){return hu_site_limit;}
    public double getFlSiteLimit(){return fl_site_limit;}
    public double getFrSiteLimit(){return fr_site_limit;}
    public double getTiv2011(){return tiv_2011;}
    public double getTiv2012(){return tiv_2012;}
    public double getEqSiteDeductible(){return eq_site_deductible;}
    public double getHuSiteDeductible(){return hu_site_deductible;}
    public double getFlSiteDeductible(){return fl_site_deductible;}
    public double getFrSiteDeductible(){return fr_site_deductible;}
    public double getPointLatitude(){return point_latitude;}
    public double getPointLongitude(){return point_longitude;}
    public String getLine(){return line;}
    public String getConstruction(){return construction;}
    public int getPointGranularity(){return point_granularity;}

    public boolean equals(Assicurazione a) {
        //si fa il confronto del solo ID,dato che è un campo univoco per ogni assicurazione
        return a.getPolicyID() == policyID;
    }

    @Override
    public boolean equals(Object o){
        boolean risultato=false;

        if(o!=null){
            if(o instanceof Assicurazione){
                return this.equals((Assicurazione)o);
            }else
                return risultato;
        }else
            return risultato;
    }

    @Override
    public String toString(){
        return "policy ID:"+policyID+"\nstate code:"+statecode+"\ncounty:"+county+"\neq site limit:"+eq_site_limit+"\nhu site limit:"+hu_site_limit+"\nfl site limit:"+fl_site_limit+"\nfr site limit:"+fr_site_limit+
                "\ntiv 2011:"+tiv_2011+"\ntiv 2012:"+tiv_2012+"\neq site deductible:"+eq_site_deductible+"\nhu site deductible:"+hu_site_deductible+"\nfl site deductible:"+fl_site_deductible+"\nfr site deductible:"+fr_site_deductible+
                "\npoint latitude:"+point_latitude+"\npoint_longitude:"+point_longitude+"\nline:"+line+"\nconstruction:"+construction+"\npoint granularity:"+point_granularity+"\n";
    }

    public int compareTo(Assicurazione a){
        return Comparator.comparing(Assicurazione::getPolicyID).compare(this, a);
    }
}
