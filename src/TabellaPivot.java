import java.util.ArrayList;

public class TabellaPivot {
    private GestioneAssicurazioni assicurazioni;
    private ArrayList<CellaTabellaPivot> tabella;


    public TabellaPivot(GestioneAssicurazioni assicurazioni){
        this.assicurazioni=assicurazioni.clone();
        this.tabella=new ArrayList<CellaTabellaPivot>();
    }

    public void conta(){
        tabella.add(new CellaTabellaPivot(assicurazioni.get(0).getConstruction(),assicurazioni.get(0).getLine()));
        int index;
        CellaTabellaPivot c;

        for(int i=1; i<assicurazioni.getSize(); i++){
            c=new CellaTabellaPivot(assicurazioni.get(i).getConstruction(),assicurazioni.get(i).getLine());
            index=tabella.indexOf(c);

            if(index != -1){
                tabella.get(index).incrementa();
            }else{
                tabella.add(c);
            }
        }

        
    }

    public int totaleConstruction(String construction){
        int sommatoria=0;

        for(CellaTabellaPivot i: tabella){
            if(i.getEtichettaRiga().matches(construction))
                sommatoria+=i.getValore();
        }

        return sommatoria;
    }

    public int totaleLine(String line){
        int sommatoria=0;

        for(CellaTabellaPivot i: tabella){
            if(i.getEtichettaColonna().matches(line))
                sommatoria+=i.getValore();
        }

        return sommatoria;
    }

    public int totaleComplessivo(){
        int sommatoria=0;

        for(CellaTabellaPivot i: tabella){
            sommatoria+=(totaleLine(i.getEtichettaColonna())+totaleConstruction(i.getEtichettaRiga()));
        }

        return sommatoria;
    }

    @Override
    public String toString(){
        String s="";

        for(CellaTabellaPivot c: tabella){
            s+=c.toString();
        }


        for(int i=0; i<tabella.size(); i++){
           s+="\nTotale generale "+tabella.get(i).getEtichettaRiga()+":"+totaleConstruction(tabella.get(i).getEtichettaRiga());
        }

        s+="\nTotale generale Commercial:"+totaleLine("Commercial");
        s+="\nTotale generale Residential:"+totaleLine("Residential");
        s+="\nTotale complessivo:"+totaleComplessivo();

        return s;
    }
}