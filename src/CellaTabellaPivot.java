public class CellaTabellaPivot {
    private String etichettaRiga;
    private String etichettaColonna;
    private int valore;

    public CellaTabellaPivot(String etichettaRiga,String etichettaColonna){
        this.etichettaRiga=etichettaRiga;
        this.etichettaColonna=etichettaColonna;
        this.valore=0;
    }

    public CellaTabellaPivot(String etichettaRiga,String etichettaColonna,int valore){
        this.etichettaRiga=etichettaRiga;
        this.etichettaColonna=etichettaColonna;
        this.valore=valore;
    }


    public CellaTabellaPivot(CellaTabellaPivot r){
        this.etichettaRiga=r.getEtichettaRiga();
        this.etichettaColonna=r.getEtichettaColonna();
        this.valore=r.getValore();
    }

    public CellaTabellaPivot clone(){return new CellaTabellaPivot(this);}


    public void setEtichettaRiga(String etichettaRiga){this.etichettaRiga=etichettaRiga;}
    public void setEtichettaColonna(String etichettaColonna){this.etichettaColonna=etichettaColonna;}
    public void setValore(int valore){this.valore=valore;}

    public String getEtichettaRiga(){return etichettaRiga;}
    public String getEtichettaColonna(){return etichettaColonna;}
    public int getValore(){return valore;}
  

    public void incrementa(){
       valore+=1;
    }

    public boolean equals(CellaTabellaPivot c){
        return c.getEtichettaColonna().matches(etichettaColonna) && c.getEtichettaRiga().matches(etichettaRiga);
    }


    @Override
    public boolean equals(Object o){
        if(o!=null){
            if(o instanceof CellaTabellaPivot){
                return this.equals((CellaTabellaPivot)o);
            }else{
                return false;
            }
        }else
            return false;
    }

    @Override
    public String toString(){
        return "\netichetta riga:"+etichettaRiga+"\netichetta colonna:"+etichettaColonna+"\nfrequenza:"+valore+"\n";
    }
}
